# This file is autogenerated. DO NOT EDIT!
#
# Modifications should be made to debian/control.in instead.
# This file is regenerated automatically in the clean target.
Source: gobject-introspection
Section: devel
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Iain Lane <laney@debian.org>, Jeremy Bicha <jbicha@debian.org>
Build-Depends: debhelper (>= 11),
               gnome-pkg-tools (>= 0.10),
               python3-dev,
               python3-distutils,
               pkg-config,
               flex,
               gtk-doc-tools (>= 1.19),
               autoconf-archive,
               bison,
               libglib2.0-dev (>= 2.58.0),
               libcairo2-dev,
               libffi-dev (>= 3.0.0),
               libtool,
               python3-mako,
               python3-markdown
Build-Depends-Indep: libglib2.0-doc
Rules-Requires-Root: no
Standards-Version: 4.1.2
Vcs-Browser: https://salsa.debian.org/gnome-team/gobject-introspection
Vcs-Git: https://salsa.debian.org/gnome-team/gobject-introspection.git
Homepage: https://wiki.gnome.org/GObjectIntrospection

Package: libgirepository-1.0-1
Section: libs
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Multi-Arch: same
Description: Library for handling GObject introspection data (runtime library)
 GObject Introspection is a project for providing machine readable
 introspection data of the API of C libraries. This introspection
 data can be used in several different use cases, for example
 automatic code generation for bindings, API verification and documentation
 generation.
 .
 GObject Introspection contains tools to generate and handle the
 introspection data.
 .
 This package contains a C library for handling the introspection data.

Package: libgirepository1.0-dev
Section: libdevel
Architecture: any
Depends: libgirepository-1.0-1 (= ${binary:Version}),
         gobject-introspection (= ${binary:Version}),
         gir1.2-glib-2.0 (= ${binary:Version}),
         gir1.2-freedesktop (= ${binary:Version}),
         libglib2.0-dev (>= 2.58.0),
         libffi-dev,
         ${shlibs:Depends},
         ${misc:Depends}
Multi-Arch: same
Suggests: libgirepository1.0-doc
Description: Library for handling GObject introspection data (development files)
 GObject Introspection is a project for providing machine readable
 introspection data of the API of C libraries. This introspection
 data can be used in several different use cases, for example
 automatic code generation for bindings, API verification and documentation
 generation.
 .
 GObject Introspection contains tools to generate and handle the
 introspection data.
 .
 This package contains the headers for the C library for handling the
 introspection data.

Package: libgirepository1.0-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Recommends: libglib2.0-doc
Multi-Arch: foreign
Description: Library for handling GObject introspection data (documentation)
 GObject Introspection is a project for providing machine readable
 introspection data of the API of C libraries. This introspection
 data can be used in several different use cases, for example
 automatic code generation for bindings, API verification and documentation
 generation.
 .
 GObject Introspection contains tools to generate and handle the
 introspection data.
 .
 This package contains the documentation for the C library for handling the
 introspection data.

Package: gobject-introspection
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
         libgirepository-1.0-1 (= ${binary:Version}),
         build-essential,
         python3-distutils,
         python3-mako,
         python3-markdown
Description: Generate interface introspection data for GObject libraries
 GObject Introspection is a project for providing machine readable
 introspection data of the API of C libraries. This introspection
 data can be used in several different use cases, for example
 automatic code generation for bindings, API verification and documentation
 generation.
 .
 GObject Introspection contains tools to generate and handle the
 introspection data.
 .
 This package contains tools for extracting introspection data from libraries
 and transforming it to different formats.

Package: gir1.2-glib-2.0
Architecture: any
Section: introspection
Depends: ${gir:Depends},
         ${misc:Depends}
Provides: gir1.2-girepository-2.0 (= ${binary:Version}),
          gir1.2-gmodule-2.0 (= ${binary:Version}),
          gir1.2-gobject-2.0 (= ${binary:Version}),
          gir1.2-gio-2.0 (= ${binary:Version})
Multi-Arch: same
Description: Introspection data for GLib, GObject, Gio and GModule
 GObject Introspection is a project for providing machine readable
 introspection data of the API of C libraries. This introspection
 data can be used in several different use cases, for example
 automatic code generation for bindings, API verification and documentation
 generation.
 .
 GObject Introspection contains tools to generate and handle the
 introspection data.
 .
 This package contains the introspection data for the GLib, GObject,
 GModule and Gio libraries.

Package: gir1.2-freedesktop
Architecture: any
Section: introspection
Depends: ${gir:Depends},
         ${misc:Depends}
Provides: gir1.2-cairo-1.0 (= ${binary:Version}),
          gir1.2-dbus-1.0 (= ${binary:Version}),
          gir1.2-dbusglib-1.0 (= ${binary:Version}),
          gir1.2-fontconfig-2.0 (= ${binary:Version}),
          gir1.2-freetype2-2.0 (= ${binary:Version}),
          gir1.2-gl-1.0 (= ${binary:Version}),
          gir1.2-libxml2-2.0 (= ${binary:Version}),
          gir1.2-xfixes-4.0 (= ${binary:Version}),
          gir1.2-xft-2.0 (= ${binary:Version}),
          gir1.2-xlib-2.0 (= ${binary:Version}),
          gir1.2-xrandr-1.3 (= ${binary:Version})
Multi-Arch: same
Description: Introspection data for some FreeDesktop components
 GObject Introspection is a project for providing machine readable
 introspection data of the API of C libraries. This introspection
 data can be used in several different use cases, for example
 automatic code generation for bindings, API verification and documentation
 generation.
 .
 GObject Introspection contains tools to generate and handle the
 introspection data.
 .
 This package contains small pieces of introspection data for the Cairo,
 FontConfig, FreeType, GL, and some XOrg libraries. They are
 distributed in this package temporarily, while the original sources
 do not include support for GObject Introspection. They are far from
 complete and only include what is necessary for other introspection
 packages to work properly.
